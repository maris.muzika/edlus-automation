// multiple-cucumber-html-reporter.d.ts

declare module 'multiple-cucumber-html-reporter' {
    const report: any; // You can use 'any' if the module doesn't provide type declarations
  
    export = report;
  }