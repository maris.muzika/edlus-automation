Feature: EDLUS general test scenarios

Scenario Outline: <ID>: Verify landing page title
    When I open EDLUS landing page
    Then verify landing page has correct title

    Examples:
      | ID     |
      | GEN-01 |

Scenario Outline: <ID>: Open functions page
    Given I open EDLUS landing page
    When click "Funkcijas" link
    Then "Funkcijas" page is open

    Examples:
      | ID     |
      | GEN-02 |

Scenario Outline: <ID>: Navigate back from functions page to landing page
    Given I open EDLUS landing page
    And click "Funkcijas" link
    And "Funkcijas" page is open
    When navigate back
    Then EDLUS landing page is open

    Examples:
      | ID     |
      | GEN-03 |

Scenario Outline: <ID>: Verify "Apply to consultation" button is visible on EDLUS landing page
    Given I open EDLUS landing page
    When click "Kas ir EDLUS" link
    And What is EDLUS page is open
    Then wait for "#kas-ir-edlus-pieteikties" element to be displayed

    Examples:
      | ID     |
      | GEN-04 |

Scenario Outline: <ID>: Open application for consultation from What is EDLUS page
    Given I open EDLUS landing page
    And click "Kas ir EDLUS" link
    And What is EDLUS page is open
    When I click "Pieteikties konsultācijai" button
    Then application pages is open

    Examples:
      | ID     |
      | GEN-05 |