@Reload
Feature: EDLUS Contact application test scenarios

    Scenario Outline: <ID>: Fill application except captcha and try to submit
        Given I open application page
        When I fill application form
            | Company Name       | Dunder Mifflin            |
            | Name               | Michael                   |
            | Surname            | Scott                     |
            | Phone Number       | 29999999                  |
            | Email              | m.scott@dundermifflin.com |
            | Comment            | no comment                |
            | Terms & Conditions | true                      |
        And I submit application form
        Then alert is displayed with text "Nepareizi ievadīts drošības kods"

        Examples:
            | ID     |
            | APL-01 |

    Scenario Outline: <ID>: Try to submit application with incorrect phone number
        Given I open application page
        When I fill application form
            | Company Name       | Dunder Mifflin            |
            | Phone Number       | 29 9                      |
            | Terms & Conditions | true                      |
        And I submit application form
        Then alert is displayed with text "Lūdzu, norādiet savu telefona numuru"

        Examples:
            | ID     |
            | APL-02 |

    Scenario Outline: <ID>: Try to submit application without company name
        Given I open application page
        When I fill application form
            | Phone Number       | 29999999                  |
            | Terms & Conditions | true                      |
        And I submit application form
        Then alert is displayed with text "Lūdzu, norādiet sava uzņēmuma nosaukumu"

        Examples:
            | ID     |
            | APL-03 |

    Scenario Outline: <ID>: Try to submit application without accepting terms and conditions
        Given I open application page
        When I fill application form
            | Company Name       | Dunder Mifflin            |
            | Phone Number       | 29999999                  |
        And I submit application form
        Then alert is displayed with text "Lūgums apstiprināt iepazīšanos ar informāciju!"

        Examples:
            | ID     |
            | APL-04 |