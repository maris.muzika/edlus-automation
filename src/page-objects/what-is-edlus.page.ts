import { $ } from '@wdio/globals'
import Page from './page.js';
import menuPanel from './panels/menu.panel.ts';


class WhatIsEdlusPage extends Page {    
    title = 'Kas ir EDLUS';

    /**
     * Selectors
     */
    whatIsEdlusContainer = '#kas-ir-edlus.in-view';
    contentTitle = '//*[@id="kas-ir-edlus"]//h1';
    applyButton = '#kas-ir-edlus-pieteikties'


    /**
     * Methods
     */
    async waitFor() {
        await $(this.whatIsEdlusContainer).waitForExist();
        await $(this.contentTitle).waitForExist();
        await $(this.applyButton).waitForExist();
        await menuPanel.waitFor();
    }

    async open () {
        await super.open('/biznesam/kas-ir-edlus-biznesam');
    }

    async clickApplyButton() {
        await (await $(this.whatIsEdlusContainer)).$(this.applyButton).waitForExist();
        await (await $(this.whatIsEdlusContainer)).$(this.applyButton).click();
    }
}

export default new WhatIsEdlusPage();