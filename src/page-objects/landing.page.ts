import { $ } from '@wdio/globals'
import Page from './page.js';
import menuPanel from './panels/menu.panel.ts';


class LandingPage extends Page {    
    title = 'LMT elektroniskā darbalaika uzskaite – EDLUS';

    /**
     * Selectors
     */
    startContainer = '#sakums.in-view';

    /**
     * Methods
     */
    async waitFor() {
        await $(this.startContainer).waitForExist();
        await menuPanel.waitFor();
    }

    async open () {
        await super.open('/');
    }
}

export default new LandingPage();