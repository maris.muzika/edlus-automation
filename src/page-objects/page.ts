import { browser } from '@wdio/globals'

export default class Page {
    title = '';
    
    async open (path: string) {
        await browser.url(path)
    }

    async verifyTitle() {
        await expect(await browser.getTitle()).toEqual(this.title);
    }
}
