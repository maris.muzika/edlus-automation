import { $ } from '@wdio/globals'


class MenuPanel {    
    /**
     * Selectors
     */
    menuContainer = '#menu-biznesam-1'
    whatIsEdlusMenuItem = '[data-id="kas-ir-edlus"]';
    howItWorksMenuItem = '[data-id="ka-strada-edlus"]';
    functionsMenuItem = '[data-id="funkcijas"]';
    costsMenuItem = '[data-id="izmaksas"]';

    /**
     * Methods
     */
    async waitFor() {
        await $(this.menuContainer).waitForExist();
        await (await $(this.menuContainer)).$(this.whatIsEdlusMenuItem).waitForExist();
        await (await $(this.menuContainer)).$(this.howItWorksMenuItem).waitForExist();
        await (await $(this.menuContainer)).$(this.functionsMenuItem).waitForExist();
        await (await $(this.menuContainer)).$(this.costsMenuItem).waitForExist();
    }

    async openFunctionPage() {
        await (await $(this.menuContainer)).$(this.functionsMenuItem).waitForExist();
        await (await $(this.menuContainer)).$(this.functionsMenuItem).click();
    }

    async openWhatIsEdlusPage() {
        await (await $(this.menuContainer)).$(this.whatIsEdlusMenuItem).waitForExist();
        await (await $(this.menuContainer)).$(this.whatIsEdlusMenuItem).click();
    }
}

export default new MenuPanel();