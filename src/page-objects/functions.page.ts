import { $ } from '@wdio/globals'
import Page from './page.js';
import menuPanel from './panels/menu.panel.ts';


class FunctionsPage extends Page {    
    title = 'Funkcijas';

    /**
     * Selectors
     */
    functionsContainer = '#funkcijas.in-view';

    /**
     * Methods
     */
    async waitFor() {
        await $(this.functionsContainer).waitForExist();
        await menuPanel.waitFor();
    }

    async open () {
        await super.open('/biznesam/funkcijas-biznesam/');
    }
}

export default new FunctionsPage();