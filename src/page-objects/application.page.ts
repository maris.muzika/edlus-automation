import { $ } from '@wdio/globals'
import Page from './page.js';


class ApplicationPage extends Page {    
    title = 'LMT | Biznesa pakalpojuma pieteikums';

    /**
     * Selectors
     */
    applicationContainer = '#page-biznesa-pakalpojuma-anketa';
    companyNameInput = '#companyId2';
    nameInput = '#nameId2';
    surnameInput = '#surnameId2';
    phoneNumberInput = '#phoneId2';
    emailInput = '#emailId2';
    commentTextarea = '#commentId2';
    captchaInput = '#form1-code.capiolla-input';
    termsCheckbox = '#checkbox-atruna';
    submitButton = '#popupBtn2';

    /**
     * Methods
     */
    async waitFor() {
        await $(this.applicationContainer).waitForExist();
        await $(this.companyNameInput).waitForExist();
        await $(this.nameInput).waitForExist();
        await $(this.surnameInput).waitForExist();
        await $(this.phoneNumberInput).waitForExist();
        await $(this.emailInput).waitForExist();
        await $(this.commentTextarea).waitForExist();
        await $(this.captchaInput).waitForExist();
        await $(this.termsCheckbox).waitForExist();
        await $(this.submitButton).waitForExist();
    }

    async open () {
        await super.open('https://bizness.lmt.lv/lv/biznesa-pakalpojuma-anketa?pageId=933&type=konsultacija&hideheader=1&hidefooter=1&eldus=1');
    }

    async fillApplicationForm(formData: string[][]) {
        for (let index = 0; index < formData.length; index++) {
            switch (formData[index][0]) {
                case "Company Name":
                    await $(this.companyNameInput).setValue(formData[index][1]);
                    break;
                case "Name":
                    await $(this.nameInput).setValue(formData[index][1]);
                    break;
                case "Surname":
                    await $(this.surnameInput).setValue(formData[index][1]);
                    break;
                case "Phone Number":
                    await $(this.phoneNumberInput).setValue(formData[index][1]);
                    break;
                case "Email":
                    await $(this.emailInput).setValue(formData[index][1]);
                    break;
                case "Comment":
                    await $(this.commentTextarea).setValue(formData[index][1]);
                    break;
                case "Captcha":
                    await $(this.captchaInput).setValue(formData[index][1]);
                    break;
                case "Terms & Conditions":
                    let checkbox = await $(this.termsCheckbox);
                    await browser.execute((checkbox) => {
                        checkbox.click();
                      }, checkbox);;
                    break
            
                default:
                    break;
            };
        }
            
        
    }

    async submit() {
        await $(this.submitButton).waitForExist();
        await $(this.submitButton).click();
    }

    async getAlertText() {
        await browser.waitUntil(() => browser.isAlertOpen(), { timeout: 5000 });

        return await browser.getAlertText();
    }
}

export default new ApplicationPage();