import { Status, After, AfterStep } from '@cucumber/cucumber';
import cucumberJson from 'wdio-cucumberjs-json-reporter';

AfterStep(async scenarioResults => {
    if(scenarioResults.result.status == Status.FAILED) {
        await cucumberJson.attach("Failde scenario screenshot");
        await cucumberJson.attach(await browser.takeScreenshot(), "image/png");
    }
});

After({ tags: '@Reload'}, async function() {
    await browser.reloadSession();
});