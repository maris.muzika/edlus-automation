import { Given, When, Then } from '@wdio/cucumber-framework';
import { expect, $ } from '@wdio/globals'
import whatIsEdlusPage from '../page-objects/what-is-edlus.page.ts';

Given(/^I open What is EDLUS page$/, async () => {
    await whatIsEdlusPage.open();
    await whatIsEdlusPage.waitFor();
});

Given(/^What is EDLUS page is open$/, async () => {
    await whatIsEdlusPage.waitFor();
});

Given(/^I click "Pieteikties konsultācijai" button$/, async () => {
    const originalWindowHandle = await browser.getWindowHandle();

    await whatIsEdlusPage.clickApplyButton();

    const allWindowHandles = await browser.getWindowHandles();
    const newTabHandle = allWindowHandles.find(handle => handle !== originalWindowHandle);

    if (newTabHandle) {
        await browser.switchToWindow(newTabHandle);
    } else {
        console.error('Application page not opened');
    }
});