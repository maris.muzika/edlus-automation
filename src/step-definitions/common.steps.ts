import { Given, When, Then } from '@wdio/cucumber-framework';
import { expect, $ } from '@wdio/globals'


Given(/^navigate back$/, async () => {
    await browser.back();
});

Given(/^debug$/, async () => {
    await browser.debug();
});

Given(/^wait for "(.*)" element to be displayed$/, async (element) => {
    await $(element).waitForExist();
    await $(element).waitForDisplayed();
});
