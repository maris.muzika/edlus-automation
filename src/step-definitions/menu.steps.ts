import { Given, When, Then } from '@wdio/cucumber-framework';
import { expect, $ } from '@wdio/globals'
import menuPanel from '../page-objects/panels/menu.panel.ts';

Given(/^click "Funkcijas" link$/, async () => {
    await menuPanel.openFunctionPage();
});

Given(/^click "Kas ir EDLUS" link$/, async () => {
    await menuPanel.openWhatIsEdlusPage();
});