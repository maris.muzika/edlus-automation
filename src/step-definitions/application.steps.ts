import { Given, When, Then } from '@wdio/cucumber-framework';
import { expect, $ } from '@wdio/globals'
import applicationPage from '../page-objects/application.page.ts';


Given(/^I open application page$/, async () => {
    await applicationPage.open();
    await applicationPage.waitFor();
});

Given(/^application pages is open$/, async () => {
    await applicationPage.waitFor();
});

Given(/^I fill application form$/, async (datatable) => {
    await applicationPage.fillApplicationForm(datatable.raw());
});

Given(/^I submit application form$/, async () => {
    await applicationPage.submit();
});

Then(/^alert is displayed with text "(.*)"$/, async (alertText) => {
    expect(alertText).toEqual(await applicationPage.getAlertText());
})