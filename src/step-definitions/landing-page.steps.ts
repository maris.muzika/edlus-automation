import { Given, When, Then } from '@wdio/cucumber-framework';
import { expect, $ } from '@wdio/globals'
import landingPage from '../page-objects/landing.page.ts';


Given(/^I open EDLUS landing page$/, async () => {
    await landingPage.open();
    await landingPage.waitFor();
});

Given(/^EDLUS landing page is open$/, async () => {
    await landingPage.waitFor();
});

Given(/^verify landing page has correct title$/, async () => {
    await landingPage.verifyTitle();
});