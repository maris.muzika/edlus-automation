## Requirements
- Node version 16 or higher
- Java version 11 or higher
- NPM or Yarn

## Setup
Use yarn or npm to install neccesary dependancies
```sh
$ npm install
```

## How to
To run all tests execute command:
```sh
$ npm test
```

To run single feature file execute command:
```sh
$ npm test -- --spec ./src/features/general.feature
```

## Report
Report can be viewed by opening `index.html` file located in `./tmp/report` folder